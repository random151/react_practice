import React from 'react';
import {Button} from 'react-bootstrap';

import ProductForm from '../components/ProductForm';

export default function AddProducts(props){
	const { index, name, edit, remove} = props;
	return(
			<div key={index}>
			  <h3>Id: Mommy{index}</h3>
			  <span>{name}</span>
			  <Button variant="info" className="me-2 ms-2" size="sm" onClick={()=>edit(index)}>Edit</Button><Button variant="danger" size="sm" onClick={() => remove(index)}>Delete</Button>
			</div>
		)
}