import React from 'react';
import {Row, Col, Form, Button} from 'react-bootstrap';

export default function Login(props){
	return(
		<div className='login'>
			<Form>
			  <Row>
			  	<Col md={6}>
			  		<Form.Group className="mb-3" controlId="formBasicEmail">
			  		  <Form.Label>Email address</Form.Label>
			  		  <Form.Control type="email" placeholder="Enter email" />
			  		</Form.Group>
			  	</Col>

			  	<Col md={6}>
			  		<Form.Group className="mb-3" controlId="formBasicPassword">
			  		  <Form.Label>Password</Form.Label>
			  		  <Form.Control type="password" placeholder="Password" />
			  		</Form.Group>
			  	</Col>

			  </Row>
			  	<div className="btn">
			  		<Button variant="info" type="submit">
			  		  Submit
			  		</Button>
			  	</div>
			</Form>
		</div>
	)
}